package b137.manuel.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args) {
        System.out.println("Leap Year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

//        System.out.println("What is your first name? \n");
//        String firstName = appScanner.nextLine();
//        System.out.println("Hello, " + firstName + "!\n");

        System.out.println("Please input year in our leap year calculator: ");
        int year = appScanner.nextInt();
        boolean leap = false;

        if(year % 4 == 0) {
            if(year % 100 == 0) {
                if(year % 400 == 0) {
                    leap = true;
                } else {
                    leap = false;
                }
            } else {
                leap = true;
            }
        } else {
            leap = false;
        }
        if(leap)
            System.out.println(year + " is a leap year.");
        else
            System.out.println(year + " is not a leap year.");
    }
}
